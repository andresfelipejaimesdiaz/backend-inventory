INSERT INTO product (reference, name) values ('01', 'Teclado'), ('02', 'Portátil'), ('03', 'Mouse');
INSERT INTO rols (name) values ('ROLE_USER'), ('ROLE_ADMIN');
INSERT INTO users (username, name, last_name, email, password, enabled) values ('FelipeUser1', 'Felipe', 'Diaz', 'afjaimes5@misena.edu.co', '12345', true);
INSERT INTO user_has_rol values (1, 1), (1, 2);