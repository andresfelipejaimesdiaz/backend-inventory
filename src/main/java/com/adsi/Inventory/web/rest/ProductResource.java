package com.adsi.Inventory.web.rest;

import com.adsi.Inventory.service.IProductService;
import com.adsi.Inventory.service.dto.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductResource {

    @Autowired
    IProductService iProductService;

    @GetMapping("/get-product")
    public ResponseEntity<List<ProductDTO>> getAll() {
        return ResponseEntity.status(200).body(iProductService.getAll());
    }

    @PostMapping("/create-product")
    public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO productDTO) {
        return new ResponseEntity<>(iProductService.save(productDTO), HttpStatus.CREATED);
    }

    @GetMapping("/get-by-reference/{reference}")
    public ResponseEntity<ProductDTO> getByReference(@PathVariable String reference) {
        return new ResponseEntity<>(iProductService.getProductByReference(reference), HttpStatus.OK);
    }
}
