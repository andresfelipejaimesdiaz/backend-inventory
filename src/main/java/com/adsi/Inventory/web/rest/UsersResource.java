package com.adsi.Inventory.web.rest;

import com.adsi.Inventory.service.IUserService;
import com.adsi.Inventory.service.dto.ProductDTO;
import com.adsi.Inventory.service.dto.UsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UsersResource {

    @Autowired
    IUserService iUserService;

    @GetMapping("/get-users")
    public ResponseEntity<List<UsersDTO>> getAllUser() {
        return ResponseEntity.status(200).body(iUserService.getUsers());
    }

    @GetMapping("/get-by-id/{id}")
    public ResponseEntity<UsersDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(iUserService.getById(id), HttpStatus.OK);
    }

    @PostMapping("/create-users")
    public ResponseEntity<?> create(@RequestBody UsersDTO usersDTO) {
        UsersDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        try {
            dto = iUserService.createUser(usersDTO);
        } catch (DataAccessException err) {
            response.put("Mensaje", "Error al guardar el usuario");
            response.put("Error", err.getMessage() + ": " + err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>("Error al guardar el usuario ", HttpStatus.BAD_REQUEST);
        }
        response.put("Mensaje", "El usuario fue creado satisfactoriamente");
        response.put("Usuario", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/update-users")
    public ResponseEntity<UsersDTO> updateUser(@RequestBody UsersDTO usersDTO) {
        return new ResponseEntity<>(iUserService.updateUser(usersDTO), HttpStatus.OK);
    }


}
