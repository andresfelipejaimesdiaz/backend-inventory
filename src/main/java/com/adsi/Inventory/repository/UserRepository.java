package com.adsi.Inventory.repository;

import com.adsi.Inventory.domain.Users;
import com.adsi.Inventory.service.dto.UsersDTO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Long> {

    public Users findByUsername(String username);
}
