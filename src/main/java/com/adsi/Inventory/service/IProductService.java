package com.adsi.Inventory.service;

import com.adsi.Inventory.service.dto.ProductDTO;

import java.util.List;

public interface IProductService {

    public List<ProductDTO> getAll();

    public ProductDTO save(ProductDTO productDTO);

    public ProductDTO getProductByReference(String reference);

}
