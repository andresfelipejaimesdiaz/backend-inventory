package com.adsi.Inventory.service.imp;

import com.adsi.Inventory.domain.Users;
import com.adsi.Inventory.repository.UserRepository;
import com.adsi.Inventory.service.IUserService;
import com.adsi.Inventory.service.dto.UsersDTO;
import com.adsi.Inventory.service.dto.UsersTransformer;
import com.adsi.Inventory.service.error.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IUsersServiceImp implements IUserService {

    private Logger logger= LoggerFactory.getLogger(IUsersServiceImp.class);

    @Autowired
    private UserRepository userRepository;


    @Override
    public List<UsersDTO> getUsers() {
        return userRepository.findAll().stream()
                .map(UsersTransformer::getUsersDTOFromUsers)
                .collect(Collectors.toList());
    }

    @Override
    public UsersDTO createUser(UsersDTO usersDTO) {
        return UsersTransformer.getUsersDTOFromUsers(userRepository.save(UsersTransformer.getUsersFromUsersDTO(usersDTO)));
    }

    @Override
    public UsersDTO updateUser(UsersDTO usersDTO) {
        Users users = UsersTransformer.getUsersFromUsersDTO(usersDTO);
        return UsersTransformer.getUsersDTOFromUsers(userRepository.save(UsersTransformer.getUsersFromUsersDTO(usersDTO)));
    }

    @Override
    public UsersDTO getById(Long id) {
        Optional<Users> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new ObjectNotFoundException("El Usuario con id " + id + " no fue encontrado");
        }
        return UsersTransformer.getUsersDTOFromUsers(user.get());
    }

    @Override
    public UserDetails findByUsername(String username) throws UsernameNotFoundException {
        Users user = userRepository.findByUsername(username);
        if (user == null) {
            logger.error("Error, username " + username + " no encontrado");
            throw  new UsernameNotFoundException("Error, username " + username + " no encontrado");
        }
        List<GrantedAuthority> authorities = user.getRols()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
        return new User(user.getUsername(), user.getPassword(), user.getEnabled(), true, true, true, authorities);
    }

}
