package com.adsi.Inventory.service.imp;

import com.adsi.Inventory.domain.Product;
import com.adsi.Inventory.repository.ProductRepository;
import com.adsi.Inventory.service.IProductService;
import com.adsi.Inventory.service.dto.ProductDTO;
import com.adsi.Inventory.service.dto.ProductTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IProductServiceImp implements IProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductDTO> getAll() {
        return productRepository.findAll().stream()
                .map(ProductTransformer::getProductDTOFromProduct)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        /*
        Product result = productRepository.save(ProductTransformer.getProductFromProductDTO(productDTO));
        return ProductTransformer.getProductDTOFromProduct(result);
        */
        return ProductTransformer.getProductDTOFromProduct(productRepository.save(ProductTransformer.getProductFromProductDTO(productDTO)));
    }

    @Override
    public ProductDTO getProductByReference(String reference) {
        return ProductTransformer.getProductDTOFromProduct(productRepository.findByReference(reference).get());
    }
}
