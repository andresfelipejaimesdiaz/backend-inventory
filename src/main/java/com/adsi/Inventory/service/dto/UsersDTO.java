package com.adsi.Inventory.service.dto;

import com.adsi.Inventory.domain.Rols;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UsersDTO implements Serializable {

    @Id
    private Long id;

    @Column(unique = true, length = 20)
    private String username;

    private String name;
    private String lastName;

    @Column(unique = true)
    private String email;

    @Column(length = 30)
    private String password;

    private Boolean enabled;

    @ManyToMany
    private List<Rols> rols;
}
