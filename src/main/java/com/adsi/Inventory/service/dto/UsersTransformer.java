package com.adsi.Inventory.service.dto;

import com.adsi.Inventory.domain.Users;

public class UsersTransformer {

    public static UsersDTO getUsersDTOFromUsers(Users users) {
        if (users == null) {
            return null;
        }

        UsersDTO dto = new UsersDTO();
        dto.setId(users.getId());
        dto.setUsername(users.getUsername());
        dto.setName(users.getName());
        dto.setLastName(users.getLastName());
        dto.setEmail(users.getEmail());
        dto.setPassword(users.getPassword());
        dto.setEnabled(users.getEnabled());
        dto.setRols(users.getRols());

        return dto;

    }

    public static Users getUsersFromUsersDTO(UsersDTO dto) {
        if (dto == null) {
            return null;
        }

        Users users = new Users();
        users.setId(dto.getId());
        users.setUsername(dto.getUsername());
        users.setName(dto.getName());
        users.setLastName(dto.getLastName());
        users.setEmail(dto.getEmail());
        users.setPassword(dto.getPassword());
        users.setEnabled(dto.getEnabled());
        users.setRols(dto.getRols());

        return users;
    }
}
