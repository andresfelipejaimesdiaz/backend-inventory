package com.adsi.Inventory.service;

import com.adsi.Inventory.domain.Users;
import com.adsi.Inventory.service.dto.UsersDTO;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface IUserService {

    List<UsersDTO> getUsers();

    UsersDTO createUser(UsersDTO usersDTO);

    UsersDTO updateUser(UsersDTO usersDTO);

    UsersDTO getById(Long id);

    UserDetails findByUsername(String username);

}
